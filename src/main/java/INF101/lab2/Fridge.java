package INF101.lab2;

import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
	int max;
	ArrayList<FridgeItem> fridgeItems = new ArrayList<>();
		
	public int nItemsInFridge() {
        return fridgeItems.size();
    }
	
	public int totalSize() {
		return 20;
	}
	
	@Override
	public boolean placeIn(FridgeItem item ) {
		boolean isPlace = false;
		if (nItemsInFridge() < totalSize()) {
			fridgeItems.add(item);
			isPlace = true;
		}
		return isPlace;
	}

	@Override
	public void takeOut(FridgeItem item) {
		if (fridgeItems.contains(item)) {
		fridgeItems.remove(item);
		}
		else {
			throw new NoSuchElementException();
		}
	}

	@Override
	public void emptyFridge() {
		for (int i = 0; i < nItemsInFridge(); i++) {
			FridgeItem item = fridgeItems.get(i);
			fridgeItems.remove(item);
			i--;
		}
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		// TODO Auto-generated method stub
		ArrayList<FridgeItem> expiredFood = new ArrayList<>();
		for (int i = 0; i < nItemsInFridge(); i++) {
			FridgeItem item = fridgeItems.get(i);
			if (item.hasExpired()) {
				expiredFood.add(item);
			}
		}
		for (FridgeItem item : expiredFood) {
			fridgeItems.remove(item);
		}
		return expiredFood;
	}
	
	
	
	
}